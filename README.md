# xdmcp

Package xdmcp is the X Display Manager Control Protocol library.


## Installation

    $ go get modernc.org/xdmcp

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/xdmcp/lib

## Documentation

[godoc.org/modernc.org/xdmcp](http://godoc.org/modernc.org/xdmcp)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxdmcp](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxdmpc)
