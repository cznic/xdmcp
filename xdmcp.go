// Copyright 2021 The Xdmcp-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package xdmcp is the X Display Manager Control Protocol library.
//
// The API si currently empty.
//
// Installation
//
// 	$ go get modernc.org/xdmpc
//
// Linking using ccgo
//
// 	$ ccgo foo.c -lmodernc.org/xdmcp/lib
//
// Documentation
//
// 	http://godoc.org/modernc.org/xdmcp
//
// Builders
//
// 	https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxdmcp
package xdmcp // import "modernc.org/xdmcp"

import (
	"fmt"
	"os"
	"runtime"
	"strings"
)

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	return fmt.Sprintf("%s:%d:%s", fn, fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TODO %s", origin(2), s) //TODOOK
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}
